package com.estech.activityresultexample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

public class FirstActivity extends AppCompatActivity {

    // variables estáticas que se corresponden a dos enteros distintos
    // se pasan como parámetro en startActivityForResult para la App sepa cuál es el resultado de vuelta
    private final static int NOMBRE = 0;
    private final static int APELLIDO = 1;

    private TextInputEditText etNombre, etApellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);

        // inicializamos vistas
        Button addNombre = findViewById(R.id.btNombre);
        Button addApellidos = findViewById(R.id.btApellido);
        etNombre = findViewById(R.id.etNombre);
        etApellido = findViewById(R.id.etApellido);

        // listener del click en el botón de añadir nombre
        addNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FirstActivity.this, SecondActivity.class);
                // Iniciamos la segunda actividad, y le indicamos que la iniciamos
                // para rellenar el nombre:
                startActivityForResult(i, NOMBRE);
            }
        });

        // listener del click en el botón de añadir apellido
        addApellidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(FirstActivity.this, SecondActivity.class);
                // Iniciamos la segunda actividad, y le indicamos que la iniciamos
                // para rellenar el apellido:
                startActivityForResult(i, APELLIDO);
            }
        });
    }

    // Este método trae la información de vuelta de la segunda actividad,
    // según el resultado que envíe la segunda actividad (OK" o "CANCELED"), se hará una cosa u otra
    // Dentro del resultado OK diferenciamos también si la segunda actividad se abrió con la variable NOMBRE o APELLIDO
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Comprobamos si el resultado de la segunda actividad es "RESULT_CANCELED".
        if (resultCode == RESULT_CANCELED) {
            // Si es así mostramos mensaje de cancelado por pantalla.
            Toast.makeText(this, "Resultado cancelado", Toast.LENGTH_SHORT)
                    .show();
        } else if (resultCode == RESULT_OK) {
            // De lo contrario, recogemos el resultado de la segunda actividad.
            // si los datos extra no son nulos
            if (data.getExtras() != null) {
                String resultado = data.getExtras().getString("RESULTADO");
                // Y tratamos el resultado en función de si se lanzó para rellenar el
                // nombre o el apellido.
                switch (requestCode) {
                    case NOMBRE:
                        etNombre.setText(resultado);
                        break;
                    case APELLIDO:
                        etApellido.setText(resultado);
                        break;
                }
            }
        } else {
            // Si se quiere recoger la opción de que no se haya producido resultado ok o cancelado
            Toast.makeText(this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
        }
    }

}
