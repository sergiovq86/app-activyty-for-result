package com.estech.activityresultexample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

public class SecondActivity extends AppCompatActivity {

    // Declaramos el EditText
    private TextInputEditText etResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        // Declaramos Buttons
        Button btAceptar = findViewById(R.id.btAceptar);
        Button btCancelar = findViewById(R.id.btCancelar);
        etResult = findViewById(R.id.etResult);

        // Listener del botón aceptar
        btAceptar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Si no está vacío recogemos el resultado.
                String resultado = etResult.getText().toString();
                if (!resultado.isEmpty()) {
                    //quitamos el error puesto en el else
                    etResult.setError(null);
                    // Recogemos el intent que ha llamado a esta actividad.
                    Intent i = getIntent();
                    // Añadimos la variable resultado como datos extra para mandar a la primera actividad
                    i.putExtra("RESULTADO", resultado);
                    // Utilizamos setResult pasando RESULT_OK y el intent
                    setResult(RESULT_OK, i);
                    // Finalizamos la Activity para volver a la anterior
                    finish();
                } else {
                    // Si no tenía nada escrito el EditText lo avisamos.
                    Toast.makeText(SecondActivity.this, "No se ha introducido nada en el campo de texto",
                            Toast.LENGTH_SHORT).show();
                    // usamos setError para marcar el error en el edittext
                    etResult.setError("No has introducido nada");
                    //le damos el foco al edittext
                    etResult.requestFocus();
                }
            }
        });

        // listener del botón cancelar
        btCancelar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Si se pulsa el botón, establecemos el resultado como cancelado.
                // no hace falta pasar datos a la primera actividad
                setResult(RESULT_CANCELED);

                // Finalizamos la Activity para volver a la anterior
                finish();
            }
        });
    }
}
